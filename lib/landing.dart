import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_massege/main.dart';
import 'package:flutter_massege/message.dart';
import 'file:///C:/Users/farshid/AndroidStudioProjects/flutter_massege/lib/shomas.dart';

class Landing extends StatefulWidget {
  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> with SingleTickerProviderStateMixin{
  Firestore fir = Firestore.instance;
  AnimationController controller ;
  Animation animation ;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(
      duration: Duration(seconds: 2),
      vsync: this,

    );
    animation = CurvedAnimation(
      parent: controller,curve: Curves.easeIn
    );
    controller.reverse(from: 1.0);
    animation.addStatusListener((status) {
      if(status == AnimationStatus.completed){
        controller.reverse(from: 1.0);
      }else if(status == AnimationStatus.dismissed){
        controller.forward();
      }
    });

    controller.addListener(() {
      setState(() {

      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("آخرین"),
              Text(
                "پیام ها",
                style: TextStyle(color: Colors.blue),
              )
            ],
          ),
        ),
        body: Column(
          children: <Widget>[
            StreamBuilder<QuerySnapshot>(
              stream: fir.collection('mas').snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.lightBlueAccent,
                    ),
                  );
                }
                final mas = snapshot.data.documents;
                List<MessageBub> masseageWidge = [];
                for (var masseage in mas) {
                  final masseageText = masseage.data['text'];
                  final masseageWigget = MessageBub(mas: masseageText,me: "",);
                  masseageWidge.add(masseageWigget);
                }
                return Expanded(
                  child: ListView(
                    padding: EdgeInsets.all(10.0),
                    children: masseageWidge,
                  ),
                );
              },
            ),
            FlatButton(

              onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context){
                    return Message();
                  }
                ));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[Text("ارسال پیام جدید"), Icon(Icons.email,size: animation.value *60,)],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
