import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'file:///C:/Users/farshid/AndroidStudioProjects/flutter_massege/lib/shomas.dart';


class Message extends StatefulWidget {


  String textmas;

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> {
  String textmf;

  final fire = Firestore.instance;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("پیام های"),
                Text("من", style: TextStyle(color: Colors.blue),)
              ],
            )
        ),
        body: Column(
          children: <Widget>[
            StreamBuilder<QuerySnapshot>(
              stream: fire.collection('mas').snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.lightBlueAccent,
                    ),
                  );
                }
                final mas = snapshot.data.documents;
                List<MessageBub> masseageWidge = [];
                for (var masseage in mas) {
                  final masseageText = masseage.data['text'];
                  final masseageWigget = MessageBub(
                    mas: masseageText, me: "من :",);
                  masseageWidge.add(masseageWigget);
                }
                return Expanded(
                  child: ListView(
                    padding: EdgeInsets.all(10.0),
                    children: masseageWidge,
                  ),
                );
              },
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.grey[300]
              ),
              child: Row(
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      fire.collection('mas').add({'text': textmf});
                    },
                    child: Icon(Icons.textsms),
                  ),
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                          fillColor: Colors.lightBlueAccent,
                          border: OutlineInputBorder(),
                          hintText: 'لطفا پیام خود را وارد کنید'),
                      textDirection: TextDirection.rtl,
                      onChanged: (value) {
                        textmf = value;
                      },
                    ),
                  ),

                ],
              ),
            ),
          ],
        ),
        // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }


}
