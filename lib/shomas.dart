import 'package:flutter/material.dart';


class MessageBub extends StatelessWidget {
  MessageBub({this.mas,this.me});

  final String mas;
  final  String me ;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(me),
          Material(
              borderRadius: BorderRadius.circular(30.0),
              color: Colors.lightBlueAccent,
              elevation: 5,
              child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                  child: Text(
                    '$mas',
                    style: TextStyle(color: Colors.white),
                  )))
        ],
      ),
    );
  }
}